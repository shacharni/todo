<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->insert([
            [
            'title' => 'Buy milk',
            'user_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
            'status'=>'0',
            ],
            [
                'title' => 'home work',
                'user_id' => 1,
                'created_at' => date('Y-m-d G:i:s'),
                'status'=>'0',
                ],
                [
                    'title' => 'walk',
                    'user_id' => 2,
                    'created_at' => date('Y-m-d G:i:s'),
                    'status'=>'0',
                    ],
            [
                'title' => 'Prepare for the test',
                'user_id' => 1,
                'created_at' => date('Y-m-d G:i:s'),
                'status'=>'0',
            ],
            [
                 'title' => 'Read a book',
                'user_id' => 2,
                'created_at' => date('Y-m-d G:i:s'),
                'status'=>'0',
            ],
        ]);
    }
}

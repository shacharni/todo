<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::table('users')->insert([
                [
                'name' => 'jack',
                'email' => 'jack@jack.com',
                'password'=>Hash::make('12345678'),
                'created_at' => date('Y-m-d G:i:s')
                ],
                [
                    'name' => 'john',
                    'email' => 'john@john.com',
                    'password'=>Hash::make('12345678'),
                    'created_at' => date('Y-m-d G:i:s')
                ]
            ]);
        } 
    }
}
